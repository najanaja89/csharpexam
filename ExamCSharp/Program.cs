﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Modules;
using Methods;
using IMethods;
using System.Runtime.Serialization.Formatters.Binary;

namespace ExamCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Registration registration = new Registration();

            //Console.WriteLine(user.Login);
            BinaryFormatter formatter = new BinaryFormatter();
            string path = @"D:\user.txt";


            while (true)
            {
                string menu = "";
                Console.WriteLine();
                Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                Console.WriteLine("Enter 1 to register user");
                Console.WriteLine("Enter 2 to sign in");
                Console.WriteLine("Enter 3 to exit");

                menu = Console.ReadLine();

                switch (menu)
                {
                    case "1":
                        Console.WriteLine();
                        Console.WriteLine("---------------------------------------------");
                        Console.WriteLine("Enter name");
                        string name = Console.ReadLine();

                        Console.WriteLine("Enter surname");
                        string sername = Console.ReadLine();

                        Console.WriteLine("Enter Login");
                        string login = Console.ReadLine();

                        Console.WriteLine("Enter password");
                        string password = Console.ReadLine();

                        var user = registration.EnterData(name, sername, login, password);

                        var listUser = new List<User>();

                        if (File.Exists(path))
                        {
                            using (FileStream writeStream = new FileStream(path, FileMode.Open))
                            {
                                listUser = (List<User>)formatter.Deserialize(writeStream);
                            }
                        }

                        foreach (var item in listUser)
                        {
                            if (item.Login == user.Login)
                            {
                                Console.WriteLine("User Exist!");
                                System.Environment.Exit(0);
                            }
                        }

                        listUser.Add(user);

                        using (FileStream writeStream = new FileStream(path, FileMode.OpenOrCreate))
                        {

                            formatter.Serialize(writeStream, listUser);
                        }

                        break;

                    case "2":
                        Console.WriteLine();
                        Console.WriteLine("**********************************************");
                        Console.WriteLine("Enter Login");
                        login = Console.ReadLine();

                        Console.WriteLine("Enter password");
                        password = Console.ReadLine();

                        using (FileStream readStream = new FileStream(path, FileMode.OpenOrCreate))
                        {
                            var DeserUser = (List<User>)formatter.Deserialize(readStream);

                            foreach (var item in DeserUser)
                            {
                                if (item.Login == login && item.Password == password)
                                {
                                    Console.WriteLine($"Welcome {item.Login}");
                                }
                            }
                        }
                        break;

                    case "3":
                        System.Environment.Exit(0);
                        break;

                    default:
                        break;
                }
            }
            Console.ReadLine();
        }
    }
}
