﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modules;

namespace IMethods
{
    public interface IRegistration
    {
        User EnterData(string name, string sername, string login, string password);
    }
}
