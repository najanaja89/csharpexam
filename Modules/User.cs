﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;



namespace Modules
{
    [Serializable]
    public class User
    {
        public string Name { get; set; }
        public string Sername { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}