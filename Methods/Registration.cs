﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMethods;
using Modules;

namespace Methods
{
    public class Registration : IRegistration
    {
        public User EnterData(string name, string sername, string login, string password)
        {
            User user = new User()
            {
                Name = name,
                Sername = sername,
                Login = login,
                Password = password
            };
            return user;
        }
    }
}
